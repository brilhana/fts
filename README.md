# fts

Featuretools using Spark to speed up feature generation.

## Installation

```bash
git clone https://gitlab.com/brilhana/fts.git
cd fts
python setup.py install
```

## Usage

Use `fts.dfs` instead of Featuretools's `fts` to run deep feature synthesis on the Spark backend.
